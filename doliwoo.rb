require 'json'
require 'yaml'
require 'woocommerce_api'
require 'rest-client'
require 'optparse'


def woocommerce_get(woocommerce, endpoint, query={})
    response = woocommerce.get(endpoint, query)
    if response.code == 200
        response = response.parsed_response
    else
        puts response.code.to_s + ' ' + response.message
        puts response['message']
        response = []
    end
    return response
end

def woocommerce_put(woocommerce, endpoint, data={})
    response = woocommerce.put(endpoint, data)
    if response.code == 200
        response = response.parsed_response
    else
        puts response.code.to_s + ' ' + response.message
        puts response['message']
        response = []
    end
    return response
end

def dolibarr_get(dolibarr, endpoint, query={})
    begin 
        response = RestClient.get(dolibarr['url']+endpoint,
            { DOLAPIKEY: dolibarr['api_key'], params:query},)
        response = JSON.parse(response)

    rescue RestClient::ExceptionWithResponse => e
        error = JSON.parse(e.response)['error'].to_a.join(' ')
        puts error
        result = false
        response = []
    end
    return response
end

def dolibarr_post(dolibarr, endpoint, data={})
    begin
        result = RestClient.post(dolibarr['url']+endpoint,data,{ DOLAPIKEY: dolibarr['api_key']})
        #RestClient.post(dolibarr['url']+endpoint,data,{ DOLAPIKEY: dolibarr['api_key'], params:data})
        # result = true
    rescue RestClient::ExceptionWithResponse => e
        error = JSON.parse(e.response)['error'].to_a.join(' ')
        puts error
        result = false
    end
    return result
end

def does_this_thirdparty_exist(dolibarr, name)
    endpoint="thirdparties?sqlfilters=((t.nom%3Alike%3A'#{name}'))"
    response = dolibarr_get(dolibarr, endpoint)
    return !response.empty? && response.length ==1
end

def import_thirdparty(dolibarr, billing)
    address = [billing['address_1'],billing['address_2']].reject { |c| c.empty? }
    address = address.join(", ")
    data = {
        name: billing['first_name']+' '+billing['last_name'], 
        name_alias:billing['company'],
        first_name: billing['first_name'], 
        last_name: billing['last_name'], 
        town: billing['city'], 
        phone: billing['phone'], 
        email: billing['phone'], 
        address: address, 
        country_code: billing['country'], 
        zip: billing['postcode'],
        client: "1"
    }
    thirdparty_id = dolibarr_post(dolibarr, "thirdparties", data)
    return thirdparty_id != false
end

def import_thirdparties(dolibarr, billings)
    result = true
    for billing in billings do
        name = billing['first_name']+' '+billing['last_name']
        if !does_this_thirdparty_exist(dolibarr, name)
            result = result and import_thirdparty(dolibarr, billing)
        end
    end
    return result
end

def get_thirdparty_id(dolibarr, name)
    endpoint="thirdparties?sqlfilters=((t.nom%3Alike%3A'#{name}'))"
    response = dolibarr_get(dolibarr, endpoint)
    thirdparty_id = response[0]['ref']
    return thirdparty_id
end

def import_products(dolibarr, products, woocommerce_url)
    result = true
    for product in products do
        label = product['name']
        if !does_this_product_exist(dolibarr, label)
            result = result and import_product(dolibarr, product, woocommerce_url)
        end
    end
    return result
end

def import_product(dolibarr, product, woocommerce_url)
    product_url=woocommerce_url+product['name'].gsub(' ','-').gsub('/','-')
    if product['product_id'] != nil
        product_id = product['product_id']
    else
        product_id=product['id']
    end
    if product['total'] != nil
        price = product['total'].to_s
    else
        price = product['price'].to_s
    end
    data = {
    ref: "woocommerce_"+product_id.to_s,
    label: product['name'],
    description: product['name'], # Il n'y a pas de description des produits dans order
    price: price, #HT
    price_ttc: price,
    url: product_url,
    status: "1",
    status_buy: "1"
    }
    product_id = dolibarr_post(dolibarr, "products", data)
    return product_id != false
end

def import_product_by_param(dolibarr, woocommerce, woocommerce_url, param)
    products = woocommerce_get(woocommerce, 'products', param)        
    import_products(dolibarr, products, woocommerce_url)
end

def does_this_product_exist(dolibarr, label)
    label = CGI.escape(label)
    endpoint="products?sqlfilters=((t.label%3A%3D%3A'#{label}'))&ids_only=true"
    response = dolibarr_get(dolibarr, endpoint)
    return !response.empty? && response.length ==1
end

def get_product_id(dolibarr, label)
    label = CGI.escape(label)
    endpoint="products?sqlfilters=((t.label%3A%3D%3A'#{label}'))&ids_only=true"
    response = dolibarr_get(dolibarr, endpoint)
    product_id=response[0]
    return product_id
end

def create_invoices(dolibarr, woocommerce, orders)
    result = true
    for order in orders do
        result = result and create_invoice(dolibarr, woocommerce, order)
    end
    return result
end

def create_invoice(dolibarr, woocommerce, order)
    name = order['billing']['first_name']+' '+order['billing']['last_name']
    thirdparty_id = get_thirdparty_id(dolibarr, name)
    data = {
        socid:thirdparty_id,
        ref_ext:order['id'],
        note_public:order['customer_note'],
        brouillon: 1
    }
    invoice_id = dolibarr_post(dolibarr, "invoices", data)
    products = order["line_items"]
    result = add_products_to_invoice(dolibarr, invoice_id, products)
    woocommerce_put(woocommerce,"orders/#{order['id']}",{status:'on-hold'})
end

def add_products_to_invoice(dolibarr, invoice_id, products)
    result = true
    for product in products do
        result = result and add_product_to_invoice(dolibarr, invoice_id, product)
    end
    return result
end

def add_product_to_invoice(dolibarr, invoice_id, product)
    label = product['name']
    product_id = get_product_id(dolibarr, label)
    data = {
        fk_product: product_id,
        label: product['name'],
        desc: product['name'],
        ref: "woocommerce_"+product['product_id'].to_s,
        libelle: product['name'],
        product_type: "0",
        product_ref: product['name'],
        product_label: product['name'],
        product_desc: product['name'],
        description: product['name'],
        qty: product['quantity'],
        total_ht: product['total'].to_s,
        total_ttc: product['total'].to_s,
        subprice: product['total'].to_s
      }
      return dolibarr_post(dolibarr, "invoices/#{invoice_id}/lines", data)
end

def update_woocommerce_orders_state(dolibarr, woocommerce)
    # https://dolitest.mithril.re/api/index.php/explorer/#!/invoices/listInvoices
    # https://woocommerce.github.io/woocommerce-rest-api-docs/#order-properties
        states = { #Dolibarr:Woocommerce
            draft:"on-hold" , 
            unpaid:"pending",
            paid:"completed", 
            cancelled:"cancelled"
        }
        states.each do |dolibarr_state, woocommerce_state|
            invoices = dolibarr_get(dolibarr,"invoices?status=#{dolibarr_state}")
            for invoice in invoices do
                order_id=invoice['ref_ext']
                woocommerce_put(woocommerce,"orders/#{order_id}",{status:woocommerce_state})
            end
        end
    end

def fast_update_woocommerce_orders_state(dolibarr, woocommerce)
    woocommerce_states = ['pending', 'processing', 'on-hold', 'cancelled', 'refunded', 'failed','trash'] # , 'completed']

    # Status de la facture dans Dolibarr
    status = {# Dolibar JSON : Dolibarr status
        "0"=>'draft',
        "1"=>'unpaid',
        "2"=>'paid',
        "3"=>"cancelled",
    }

    states = { #Dolibarr:Woocommerce
        "draft"=>"on-hold" , 
        "unpaid"=>"pending",
        "paid"=>"completed", 
        "cancelled"=>"cancelled"
    }

    for woocommerce_state in woocommerce_states do
        orders = woocommerce_get(woocommerce, 'orders', {status: woocommerce_state})
        puts woocommerce_state
        for order in orders do
            puts order['id']
            invoice = dolibarr_get(dolibarr,"invoices/ref_ext/#{order['id']}?contact_list=1")  
            new_woocommerce_state = states[status[invoice['status']]] # :-(   
            puts new_woocommerce_state
            woocommerce_put(woocommerce,"orders/#{order['id']}",{status:new_woocommerce_state})
        end
    end

end



config = YAML.load_file('./config/config.yml')

dolibarr = config['dolibarr']
woocommerce = WooCommerce::API.new(
            config['woocommerce']['url'],
            config['woocommerce']['api_ck'],
            config['woocommerce']['api_cs'],
            {
                wp_api: true,
                version: "wc/v3"
            }
        )


if __FILE__ == $0
    options = {}
    ARGV.push('-h') if ARGV.empty?
    OptionParser.new do |opts|
        opts.banner = "Usage : "+$PROGRAM_NAME+ " options"

        opts.on("-p", "--add-new-products", "Àjouter les nouveaux produits des commandes en cours de Woocommerce dans Dolibarr.") do |v|
            orders = woocommerce_get(woocommerce, 'orders', {status:'processing'})        
            products = orders.collect {|item| item['line_items'] }
            products.flatten!(1)
            woocommerce_url = config['woocommerce']['url']
            import_products(dolibarr, products, woocommerce_url)
        end

        opts.on("-t", "--add-new-thirdparties", "Àjouter les nouveaux clients des commandes en cours de Woocommerce dans Dolibarr.") do |v|
            orders = woocommerce_get(woocommerce, 'orders', {status:'processing'})
            billings = orders.collect {|item| item['billing'] }
            import_thirdparties(dolibarr, billings)
        end

        opts.on("-i", "--add-new-invoices", "Créer les nouvelles factures des commandes en cours de Woocommerce dans Dolibarr.") do |v|
            orders = woocommerce_get(woocommerce, 'orders', {status:'processing'})
            create_invoices(dolibarr, woocommerce, orders)
        end

        opts.on("-s", "--synchronization", "Àjouter les nouveaux clients, àjouter les nouveaux produits et créer les nouvelles factures des commandes en cours de Woocommerce dans Dolibarr.") do |v|
            orders = woocommerce_get(woocommerce, 'orders', {status:'processing'})
            billings = orders.collect {|item| item['billing'] }
            import_thirdparties(dolibarr, billings)

            products = orders.collect {|item| item['line_items'] }
            products.flatten!(1)
            woocommerce_url = config['woocommerce']['url']
            import_products(dolibarr, products, woocommerce_url)

            create_invoices(dolibarr, woocommerce, orders)
            
            update_woocommerce_orders_state(dolibarr, woocommerce)

        end

        opts.on("-u", "--update-orders-status", "Mise à jour du statut de la commande dans Woocommerce quand celui ci-est modifié dans Dolibarr") do |v|
            update_woocommerce_orders_state(dolibarr, woocommerce)

        end

        opts.on("-f", "--fast-update-orders-status", "Mise à jour du statut de la commande dans Woocommerce quand celui ci-est modifié dans Dolibarr mais à partir des commandes qui ne sont pas dans un état terminé dans Woocommerce") do |v|
            fast_update_woocommerce_orders_state(dolibarr, woocommerce)
        end

        opts.on("-r", "--import-product-by-id [ID]", "Importation d'un produit par ID") do |v|
            woocommerce_url = config['woocommerce']['url']
            import_product_by_param(dolibarr, woocommerce, woocommerce_url, {include:v})
        end
    end.parse!
end
