# Doliwoo
Synchronisation des commandes et des factures entre Woocommerce et Dolibarr

## Getting started
La synchronisation se fait via les APIs de Woocommerce et de Dolibarr. Il faut renseigner les clés APIs dans `config/config.yml`.

Dans Woocommerce une commande peut être dans plussieurs états.

`En cours (processing)` : la commande n'a pas encore été importée dans Dolibarr.

`En attente (on-hold)`  : la commande a été importée dans Dolibarr.

## Installation 
Il faut installer `Ruby 2.7`.

Installation des dépendances ruby `bundle intall`.

Mise à jour des dépendances  ruby `bundle update`.

## Licence
TODO


